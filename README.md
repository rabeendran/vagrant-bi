# BI Vagrant Umgebung
Mit hilfe dieser Vagrant-Umgebung ist es einfach möglich, dass du dein eigenes, kleines, virtuelles Rechenzentrum in wenigen Schritten in Betrieb nehmen kannst. Alles was du dazu benötigst, erfährst du in der folgenden Anleitung.

## 1. Voraussetzungen
Um die Vagrant-Umgebung auf deinem Testgerät verwenden zu können, musst du die folgenden Voraussetzungen erfüllen: 
1. Stelle sicher, dass Virtualisierung im BIOS deines Testgerätes aktiviert ist. Falls du dir nicht sicher bist wie du die Virtualisierung aktivieren kannst, versuche im Internet danach zu suchen, oder frage deinen Praxisbildner.
2. Das Programm VirtualBox, welches du [hier](https://www.virtualbox.org/wiki/Downloads "VirtualBox Download") herunterladen kannst.
3. Das Programm Vagrant, welches [hier](https://www.vagrantup.com/downloads "Vagrant Download") heruntergeladen werden kann.
4. Genügend freier Festplattenspeicher. Die Windows-Only Umgebung benötigt um die 40 bis 50 GB.

## 2. Erstmalige Installation
Wenn du alle Voraussetzungen aus Schritt 1 erfüllt hast, kannst du mit der Installation der Umgebung beginnen. Dazu klonst du dieses Repository auf dein Testgerät oder lädst das Zip herunter und entpackst es z.B. auf deinen Desktop oder einen anderen Ort, an dem du es gut finden kannst. Befolge nun folgende Schritte:

1. Starte ein Terminal (CMD oder Powershell) und navigiere mittels dem Befehl `cd` an den Ort, wo du das Repository gespeichert hast. Beispiel: `cd Desktop\vagrant-umgebung-la`
2. Überpüfe mittels dem Befehl `dir` ob du einen Ordner namens `windows` und einen namens `windows-linux` siehst. Ist dies der Fall, kannst du mit dem nächsten Schritt fortfahren.
3. Wechsle nur mittels `cd windows` resp. `cd windows-linux` in das Verzcheinis der entsprechenden Umgebung.
4. Starte nun mittels `vagrant up` die Umgebung. Dies kann je nach Internetgeschwindigkeit und Leistung deines Gerätes bis zu 90 Minuten dauern. Du kannst aber gut etwas anderes während dieser Zeit machen. 
5. Wie du mit der entsprechenden Umgebung arbeiten kannst, erfärst du im Readme der entsprechenden Umgebung. Viel Spass! ;)