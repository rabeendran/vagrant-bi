#!/bin/bash
domain=vagrant.lab
adpasswd=Welcome\!20
echo "Installing xrdp"
sudo apt update -y > /dev/null 2>&1
sudo apt install xrdp realmd sssd-tools adcli sssd libnss-sss libpam-sss -y > /dev/null 2>&1
sudo systemctl enable xrdp
sudo systemctl start xrdp
echo "Setting DNS server to 192.168.20.10"
sudo systemd-resolve --set-dns=192.168.20.10 --interface=eth1
echo "Joining domain $domain, please wait"
echo "session optional pam_mkhomedir.so" > /etc/pam.d/common-session
    cat <<EOT >> /etc/realmd.conf
[users]
default-home = /home/%d/%U
default-shell = /bin/bash
[active-directory]
default-client = sssd
os-name = Ubuntu 20.04 Desktop
os-version = 20.04
[service]
automatic-install = no
[vagrant.lab]
fully-qualified-names = no
automatic-id-mapping = yes
user-principal = yes
manage-system = no
#computer-ou = Computers
EOT
cat <<EOT >> /etc/krb5.conf
[libdefaults]
 dns_lookup_realm = false
 ticket_lifetime = 24h
 renew_lifetime = 7d
 forwardable = true
 rdns = false
 default_realm = VAGRANT.LAB
 default_ccache_name = KEYRING:persistent:%{uid}
EOT
echo $adpasswd | sudo realm join -U Administrator $domain
realm permit --all
