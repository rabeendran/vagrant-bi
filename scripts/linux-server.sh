#!/bin/bash
#get OS
domain=vagrant.lab
adpasswd=Welcome\!20
if [ -f /etc/os-release ];then
    . /etc/os-release
    OS=$ID
else
    echo "Error, could not get OS, please check the script"
    echo "Aborting"
    exit 1
fi
if [ $OS == "fedora" ];then
    echo "Installing software"
     dnf install sssd realmd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation openldap-clients policycoreutils -y > /dev/null 2>&1
    echo "Creating AD config file"
     touch /etc/realmd.conf
     echo "session optional pam_mkhomedir.so" >> /etc/pam.d/config-util
    cat <<EOT >> /etc/realmd.conf
[users]
default-home = /home/%d/%U
default-shell = /bin/bash
[active-directory]
default-client = sssd
os-name = Fedora 32 Server
os-version = 32
[service]
automatic-install = no
[vagrant.lab]
fully-qualified-names = no
automatic-id-mapping = yes
user-principal = yes
manage-system = no
#computer-ou = Computers
EOT
    echo "Setting DNS server"
    echo "nameserver 192.168.20.10" > /etc/resolv.conf
    echo "Joining domain $domain, please wait"
    echo $adpasswd |  realm join -U Administrator $domain
    realm permit --all
    echo "%domain\ users ALL=(ALL:ALL) ALL" >> /etc/sudoers

elif [ $OS == "ubuntu" ];then
    echo "Installing software"
     apt update -y > /dev/null 2>&1
     apt install realmd sssd-tools adcli sssd libnss-sss libpam-sss resolvconf policykit-1 samba-common-bin packagekit -y  > /dev/null 2>&1
    echo "Creating AD config file"
     touch /etc/realmd.conf
     echo "session optional pam_mkhomedir.so" >> /etc/pam.d/common-session
    cat <<EOT >> /etc/realmd.conf
[users]
default-home = /home/%d/%U
default-shell = /bin/bash
[active-directory]
default-client = sssd
os-name = Ubuntu 18.04 Server
os-version = 18.04
[service]
automatic-install = no
[vagrant.lab]
fully-qualified-names = no
automatic-id-mapping = yes
user-principal = yes
manage-system = no
#computer-ou = Computers
EOT
    echo "Setting DNS server to 192.168.20.10"
    echo "nameserver 192.168.20.10" > /etc/resolvconf/resolv.conf.d/head
     systemctl enable resolvconf
     systemctl start resolvconf
    echo "Joining domain $domain, please wait"
    echo $adpasswd |  realm join -U Administrator $domain
    realm permit --all
    echo "%domain\ users ALL=(ALL:ALL) ALL" >> /etc/sudoers
else
    echo "Could not proceed because of unknown OS"
    exit 1
fi


